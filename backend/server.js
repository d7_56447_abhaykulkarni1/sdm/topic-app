// get the dependencies
const db = require("./db");
const utils = require("./utils");
const cors = require("cors");
const express = require("express");

// create node app
const app = express();

// add the app use
app.use(express.json());

app.use(cors("*"));

// create apis
app.get("/topics", (req, resp) => {
  // create statement
  const sql = `select * from topics`;
  db.execute(sql, (err, data) => {
    resp.send(utils.createresult(err, data));
  });
});

app.post("/topics", (req, resp) => {
  // create statement
  const { name } = req.body;
  const sql = `insert into topics (name) values('${name}')`;
  db.execute(sql, (err, data) => {
    resp.send(utils.createresult(err, data));
  });
});

app.delete("/topics/:id", (req, resp) => {
  // create statement
  const { id } = req.params;
  const sql = `Delete from topics where id = ${id}`;
  db.execute(sql, (err, data) => {
    resp.send(utils.createresult(err, data));
  });
});

app.listen(4001, "0.0.0.0", () => {
  console.log("Server started at 4001");
});
